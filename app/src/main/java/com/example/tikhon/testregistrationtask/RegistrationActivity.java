package com.example.tikhon.testregistrationtask;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class RegistrationActivity extends AppCompatActivity implements PhotoPicker.OnPhotoPickerListener {

    private List<TextInputLayout> textInputLayoutList = new ArrayList<>();
    private PhotoPicker photoPicker;
    private ImageAdapter imageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        photoPicker = new PhotoPicker(this, this, null);
        initRecyclerViewImages();
        initCityAutocompleteView();
        fillTextInputLayoutList();
        findViewById(R.id.btnRegistration).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doRegistration();
            }
        });
        findViewById(R.id.btnPickPhoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickPhoto();
            }
        });
    }

    private void initRecyclerViewImages() {
        RecyclerView rvImages = (RecyclerView) findViewById(R.id.rvPhotos);
        ItemOffsetDecoration decoration = new ItemOffsetDecoration(this, R.dimen.rv_default_item_spacing);
        rvImages.addItemDecoration(decoration);
        rvImages.setLayoutManager(new GridLayoutManager(this, 2));
        rvImages.setNestedScrollingEnabled(false);
        imageAdapter = new ImageAdapter();
        rvImages.setAdapter(imageAdapter);
    }

    private void pickPhoto() {
        photoPicker.requestGalleryIntent();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        photoPicker.onActivityResult(requestCode, resultCode, data);
    }

    private void initCityAutocompleteView() {
        AutoCompleteTextView ac = (AutoCompleteTextView) findViewById(R.id.acTvCity);
        String[] cities = getResources().getStringArray(R.array.cities);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, cities);
        ac.setThreshold(3);
        ac.setAdapter(adapter);
    }

    private void fillTextInputLayoutList() {
        textInputLayoutList.add((TextInputLayout) findViewById(R.id.tilFirstName));
        textInputLayoutList.add((TextInputLayout) findViewById(R.id.tilLastName));
        textInputLayoutList.add((TextInputLayout) findViewById(R.id.tilPhone));
        textInputLayoutList.add((TextInputLayout) findViewById(R.id.tilCountry));
        textInputLayoutList.add((TextInputLayout) findViewById(R.id.tilCity));
        textInputLayoutList.add((TextInputLayout) findViewById(R.id.tilEmail));
    }

    private void doRegistration() {
        boolean noEmptyFields = true;
        boolean fieldIsValid;
        EditText editText;
        String errorText = getString(R.string.registration_error);
        for (TextInputLayout til : textInputLayoutList) {
             editText = til.getEditText();
            if (editText instanceof PhoneMaskedEditText) {
                PhoneMaskedEditText pmet = (PhoneMaskedEditText) editText;
                fieldIsValid = pmet.isPhoneNumberCorrect();
            } else {
                fieldIsValid = editText.getText().length() > 0;
            }
            if (fieldIsValid) {
                til.setErrorEnabled(false);
            } else {
                til.setErrorEnabled(true);
                til.setError(errorText);
                noEmptyFields = false;
            }
        }
        if (noEmptyFields) {
            showSuccessDialog();
        }
    }

    private void showSuccessDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.registration_success)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        clearFields();
                    }
                })
                .setCancelable(false)
                .show();
    }

    private void clearFields() {
        EditText editText;
        for (TextInputLayout til : textInputLayoutList) {
            editText = til.getEditText();
            if (editText instanceof PhoneMaskedEditText) {
                ((PhoneMaskedEditText) editText).clearPhoneNumber();
            } else {
                editText.setText(null);
            }
        }
        imageAdapter.clear();
    }

    @Override
    public void onPhotoPicked(File photoFile) {
        imageAdapter.addItem(photoFile);
    }
}
