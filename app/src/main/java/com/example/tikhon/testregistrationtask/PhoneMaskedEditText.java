package com.example.tikhon.testregistrationtask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;

import java.io.Serializable;


/**
 * @author =Troy= <Daniel Serdyukov>
 * @version 1.0
 */
public class PhoneMaskedEditText extends AppCompatEditText implements Serializable {
    /**
     * Request code that is used to open contacts list
     */
    public static final int PICK_CONTACT = 1489;
    /**
     * Phone pattern for russian phones.
     */
    private static final String MOBILE_PATTERN_DEFAULT = "7 (___) ___-__-__";

    /**
     * Default prefix.
     */
    private static final String PHONE_PREFIX = "7";

    /**
     * The character that used for mask.
     */
    protected static final char MASK_CHAR = '_';
    /**
     * The current instance of mobile pattern.
     */
    private String mMobilePattern;
    /**
     * The original phone number.
     */
    private String mPhoneNumber = "";
    /**
     * The max number of Digits.
     */
    private int mMaxNumberLength;
    /**
     * Listener to call if contacts icon was clicked
     */
    private View.OnClickListener mPickContactsListener;

    public PhoneMaskedEditText(Context context, AttributeSet attrs) {
        this(context, attrs, MOBILE_PATTERN_DEFAULT);
    }

    protected PhoneMaskedEditText(Context context, AttributeSet attrs, String mobilePattern) {
        super(context, attrs);

        // apply hint text - patten + select mobile pattern.
        changeText(mobilePattern);
        setMobilePattern(mobilePattern);

        // Do not proceed select actions by long click event.
        setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // Do nothing.
                return true;
            }
        });

        // This watcher should make changes for result input data.
        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Do nothing.
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Do nothing.
            }

            @Override
            public void afterTextChanged(Editable s) {
                // get only digits And apply some filters for phone number.
                mPhoneNumber = onPreparePhoneNumber(s.toString().replaceAll("[^0-9]", ""));

                // the phone number should has fixed size.
                if (mPhoneNumber.length() > mMaxNumberLength) {
                    mPhoneNumber = mPhoneNumber.substring(0, mMaxNumberLength);
                }

                StringBuilder resultText = new StringBuilder();
                int numberIndex = 0;
                char currentChar;
                for (int index = 0; index < mMobilePattern.length(); index++) {
                    currentChar = mMobilePattern.charAt(index);
                    if (numberIndex < mPhoneNumber.length() && MASK_CHAR == currentChar) {
                        // replace char with number.
                        resultText.append(mPhoneNumber.charAt(numberIndex++));

                    } else {
                        // do not replace char.
                        resultText.append(currentChar);
                    }
                }

                // update text with current pattern.
                removeTextChangedListener(this);
                changeText(resultText.toString());
                addTextChangedListener(this);
            }
        });
    }

    /**
     * Call this method when a new pattern should be set.
     *
     * @param mobilePattern a new pattern for phone number
     */
    protected void setMobilePattern(String mobilePattern) {
        mMobilePattern = mobilePattern;

        // calculate max number length.
        mMaxNumberLength = 0;
        for (int index = 0; index < mMobilePattern.length(); index++) {
            if (MASK_CHAR == mMobilePattern.charAt(index)) {
                mMaxNumberLength++;
            }
        }

        // The pattern has changed => the caret position should be updated also.
        updateCaretToLatestGap();
    }

    /**
     * Call for prepare phone number.
     *
     * @param phoneNumber the phone number to prepare.
     * @return The prepared phone number.
     */
    protected String onPreparePhoneNumber(String phoneNumber) {
        // remove russian country code from phone number.
        if (phoneNumber.startsWith("7") || phoneNumber.startsWith("8")) {
            phoneNumber = phoneNumber.substring(1);
        }

        return phoneNumber;
    }

    public void setOnContactPickListener(final View.OnClickListener listener) {
        mPickContactsListener = listener;
    }

    public void pickContacts(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
        activity.startActivityForResult(intent, PICK_CONTACT);
    }

    /**
     * @return The value of original phone number.
     */
    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    /**
     * @return The value of original phone number with prefix.
     */
    public String getPhoneNumberWithPrefix() {
        return PHONE_PREFIX + mPhoneNumber;
    }

    /**
     * @return TRUE - the phone number is correct.
     */
    public boolean isPhoneNumberCorrect() {
        return !TextUtils.isEmpty(mPhoneNumber)
                && TextUtils.isDigitsOnly(mPhoneNumber)
                && mPhoneNumber.length() == mMaxNumberLength;
    }

    /**
     * Helper method for change current text and change caret position.
     *
     * @param text the new text.
     */
    public void changeText(CharSequence text) {
        super.setText(text);
        // select the current position.
        updateCaretToLatestGap();
    }

    public void clearPhoneNumber() {
        setText(mMobilePattern);
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        updateCaretToLatestGap();
    }

    /**
     * Helper method for select
     */
    private void updateCaretToLatestGap() {
        String currentText = getText().toString();
        int index;
        if (TextUtils.isEmpty(mPhoneNumber)) {
            index = currentText.indexOf(MASK_CHAR);
        } else {
            // the cursor will the after the latest number.
            index = currentText.lastIndexOf(mPhoneNumber.charAt(mPhoneNumber.length() - 1)) + 1;
        }

        // fix some cases when text contains some errors or empty.
        if (index < 0) {
            index = currentText.length();
        }

        // update selection when something is wrong.
        if (getSelectionStart() != index || getSelectionEnd() != index) {
            setSelection(index, index);
        }
    }

    public boolean isTextEmpty() {
        return TextUtils.isEmpty(getText()) || getText().toString().equals(MOBILE_PATTERN_DEFAULT);
    }
}